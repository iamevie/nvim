require "options"

local keys = require "keys"
keys.set_leader(" ")

keys.map("n", "<Leader>t", ":sp<Cr><C-w>J20<C-w>_:terminal<Cr>i", "Terminal")

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require "lazy".setup({
	-- colorschemes
	"Verf/deepwhite.nvim",
	"blazkowolf/gruber-darker.nvim",
	"ellisonleao/gruvbox.nvim",
	"catppuccin/nvim",
	"nyoom-engineering/oxocarbon.nvim",
	"rebelot/kanagawa.nvim",
	"Everblush/everblush.vim",
	"mstcl/ivory",

	"chomosuke/term-edit.nvim",

	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		dependencies = {
			"nvim-treesitter/nvim-treesitter-textobjects",
		},
	},
	"nvim-lua/plenary.nvim",

	"williamboman/mason.nvim",
	"williamboman/mason-lspconfig.nvim",
	"neovim/nvim-lspconfig",

	"nvim-treesitter/nvim-treesitter-context",

	"Cassin01/wf.nvim",
	"stevearc/oil.nvim",
	"MagicDuck/grug-far.nvim",
	"ThePrimeagen/refactoring.nvim",

	"Olical/conjure",
})

require "colorschemes".set_colorschemes({
	"deepwhite",
	"catppuccin-latte",
	"venom",
	"gruber-darker",
	"gruvbox",
	"oxocarbon",
	"kanagawa",
})

require "nvim-treesitter.configs".setup({
	ensure_installed = { "rust", "c", "lua", "nix", "cpp", "python" },
	highlight = { enable = true },
	indent = { enable = true },
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = "<C-space>",
			node_incremental = "<C-n>",
			node_decremental = "<C-p>",
			scope_incremental = "<C-space>",
		},
	},
	textobjects = {
		select = {
			enable = true,
			lookahead = true,
			keymaps = {
				["a="] = { query = "@assignment.outer", desc = "Select outer part of an assignment" },
				["i="] = { query = "@assignment.inner", desc = "Select inner part of an assignment" },
				["l="] = { query = "@assignment.lhs", desc = "Select left hand side of an assignment" },
				["r="] = { query = "@assignment.rhs", desc = "Select right hand side of an assignment" },

				["aa"] = { query = "@parameter.outer", desc = "Select outer part of a parameter/argument" },
				["ia"] = { query = "@parameter.inner", desc = "Select inner part of a parameter/argument" },

				["ai"] = { query = "@conditional.outer", desc = "Select outer part of a conditional" },
				["ii"] = { query = "@conditional.inner", desc = "Select inner part of a conditional" },

				["al"] = { query = "@loop.outer", desc = "Select outer part of a loop" },
				["il"] = { query = "@loop.inner", desc = "Select inner part of a loop" },

				["af"] = { query = "@call.outer", desc = "Select outer part of a function call" },
				["if"] = { query = "@call.inner", desc = "Select inner part of a function call" },

				["am"] = { query = "@function.outer", desc = "Select outer part of a method/function definition" },
				["im"] = { query = "@function.inner", desc = "Select inner part of a method/function definition" },

				["ac"] = { query = "@class.outer", desc = "Select outer part of a class" },
				["ic"] = { query = "@class.inner", desc = "Select inner part of a class" },
			},
		},
		swap = {
			enable = true,
			swap_next = {
				["<leader>na"] = "@parameter.inner",
				["<leader>nm"] = "@function.outer",
			},
			swap_previous = {
				["<leader>pa"] = "@parameter.inner",
				["<leader>pm"] = "@function.outer",
			},
		},
		move = {
			enable = true,
			set_jumps = true,
			goto_next_start = {
				["]f"] = { query = "@call.outer", desc = "Next function call start" },
				["]m"] = { query = "@function.outer", desc = "Next method/function def start" },
				["]c"] = { query = "@class.outer", desc = "Next class start" },
				["]i"] = { query = "@conditional.outer", desc = "Next conditional start" },
				["]l"] = { query = "@loop.outer", desc = "Next loop start" },
			},
			goto_next_end = {
				["]F"] = { query = "@call.outer", desc = "Next function call end" },
				["]M"] = { query = "@function.outer", desc = "Next method/function def end" },
				["]C"] = { query = "@class.outer", desc = "Next class end" },
				["]I"] = { query = "@conditional.outer", desc = "Next conditional end" },
				["]L"] = { query = "@loop.outer", desc = "Next loop end" },
			},
			goto_previous_start = {
				["[f"] = { query = "@call.outer", desc = "Prev function call start" },
				["[m"] = { query = "@function.outer", desc = "Prev method/function def start" },
				["[c"] = { query = "@class.outer", desc = "Prev class start" },
				["[i"] = { query = "@conditional.outer", desc = "Prev conditional start" },
				["[l"] = { query = "@loop.outer", desc = "Prev loop start" },
			},
			goto_previous_end = {
				["[F"] = { query = "@call.outer", desc = "Prev function call end" },
				["[M"] = { query = "@function.outer", desc = "Prev method/function def end" },
				["[C"] = { query = "@class.outer", desc = "Prev class end" },
				["[I"] = { query = "@conditional.outer", desc = "Prev conditional end" },
				["[L"] = { query = "@loop.outer", desc = "Prev loop end" },
			},
		},
	},
})

require "treesitter-context".setup({
	enable = true,
	mode = "cursor",
	separator = "-",
})

local ts_repeat_move = require "nvim-treesitter.textobjects.repeatable_move"
keys.map({ "n", "x", "o" }, ";", ts_repeat_move.repeat_last_move)
keys.map({ "n", "x", "o" }, ",", ts_repeat_move.repeat_last_move_opposite)
keys.map({ "n", "x", "o" }, "f", ts_repeat_move.builtin_f)
keys.map({ "n", "x", "o" }, "F", ts_repeat_move.builtin_F)
keys.map({ "n", "x", "o" }, "t", ts_repeat_move.builtin_t)
keys.map({ "n", "x", "o" }, "T", ts_repeat_move.builtin_T)

require "mason".setup()
require "mason-lspconfig".setup()
require "mason-lspconfig".setup_handlers {
	function (server_name)
		require ("lspconfig")[server_name].setup {}
	end
}

require "term-edit".setup({ prompt_end = "%$ " })

require "wf".setup({ theme = "chad" })

local which_key =  require "wf.builtin.which_key"
keys.map("n", "<Leader>", which_key({ text_insert_in_advance = "<Leader>" }), "[wf.nvim] which-key /")

local mark = require "wf.builtin.mark"
keys.map("n", "'", mark(), "[wf.nvim] mark")

local buffer = require "wf.builtin.buffer"
keys.map("n", "<C-b>", buffer(), "[wf.nvim] buffer")

local register = require "wf.builtin.register"
keys.map("n", '"', register(), "[wf.nvim] register")

require "oil".setup({
	default_file_explorer = true,
	columns = { "permissions", "size", "mtime" },
	experimental_watch_for_changes = true,
})
keys.map("n", "-", "<Cmd>Oil<Cr>", "Open parent directory")

require "grug-far".setup()
keys.map("n", "<Leader>s", "<Cmd>GrugFar<Cr>", "Search and Replace")

require "refactoring".setup()
