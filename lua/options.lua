local opts = {
	termguicolors	= true,
	shiftwidth		= 4,
	tabstop			= 4,
	expandtab		= false,
	swapfile 		= false,
	showmode		= true,
	writebackup		= false,
	undofile		= true,
	splitbelow		= true,
	splitright		= true,
	ignorecase		= true,
	smartcase		= true,
	list			= true,
	cursorline		= true,
	confirm			= true,
	timeoutlen		= 0,
	conceallevel	= 1,
	laststatus		= 3,
	showtabline		= 1,
	showcmdloc		= "statusline",
	splitkeep		= "screen",
	backspace		= "indent,eol,nostop",
	foldmethod		= "syntax",
	viewoptions		= "cursor,folds",

	number			= true,
	relativenumber	= true,
	foldcolumn		= "1",
	signcolumn		= "no",

	scrolloff		= 8,
}

for opt, val in pairs(opts) do
	vim.o[opt] = val
end
