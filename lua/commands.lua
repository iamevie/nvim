local M = {}

M.set_command = function(command, action)
	vim.api.nvim_create_user_command(command, action, {})
end

return M
