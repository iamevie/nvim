fn main() {
    other("Duck");
}

fn other<S: Into<String> + std::fmt::Display>(name: S) -> String {
    format!("Hello, {name}!")
}
